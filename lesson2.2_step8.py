from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import os


try: 
    link = " http://suninjuly.github.io/file_input.html"
    browser = webdriver.Chrome()
    browser.get(link)

    input1 = browser.find_element(By.NAME, "firstname")
    input1.send_keys("Ivan")
    
    input2 = browser.find_element(By.NAME, "lastname")
    input2.send_keys("Ivanov")
    
    input3 = browser.find_element(By.NAME, "email")
    input3.send_keys("IvanIvanow@gmail.com")
        
    time.sleep(1)
    
    # Загружаем на страницу файл file.txt
    current_dir = os.path.abspath(os.path.dirname(__file__))
    file_path = os.path.join(current_dir, 'file.txt')
    
    input_file = browser.find_element(By.NAME, "file")
    input_file.send_keys(file_path)
    
    time.sleep(1)
    
    # Ищем кнопку
    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button.click()


finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()
    