from selenium import webdriver
from selenium.webdriver.common.by import By
import time

import math

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

try: 
    link = "https://SunInJuly.github.io/execute_script.html"
    browser = webdriver.Chrome()
    browser.get(link)

    x_element = browser.find_element(By.ID, "input_value")
    x = x_element.text
    y = calc(x)
    
    input1 = browser.find_element(By.ID, "answer")
    input1.send_keys(y)
        
    time.sleep(1)
    
    # Ищем кнопку
    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    # Скролим страницу
    browser.execute_script("return arguments[0].scrollIntoView(true);", button)
    
    option1 = browser.find_element(By.ID, "robotCheckbox")
    option1.click()
    
        
    option2 = browser.find_element(By.ID, "robotsRule")
    option2.click()
    
    time.sleep(1)
    
    button.click()


finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()
    