from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import math


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/explicit_wait2.html"
    browser = webdriver.Chrome()
    browser.get(link)

    # Ищем кнопку
    button = browser.find_element(By.ID, "book")
    # Ждем пока цена не станет 100
    WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, "price"), "100"))
    # После этого нажимаем кнопку
    button.click()

    x_element = browser.find_element(By.ID, "input_value")
    x = x_element.text

    input_value = browser.find_element(By.ID, "answer")
    input_value.send_keys(calc(x))

    submit_button = browser.find_element(By.ID, "solve")
    submit_button.click()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
