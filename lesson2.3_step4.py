from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import os
import math


def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))


try: 
    link = "http://suninjuly.github.io/alert_accept.html"
    browser = webdriver.Chrome()
    browser.get(link)
  
    # Ищем кнопку
    button1 = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button1.click()
    
    time.sleep(1)
    
    alert = browser.switch_to.alert
    alert.accept()
    
    x_element = browser.find_element(By.ID, "input_value")
    x = x_element.text
    
    input_value = browser.find_element(By.ID, "answer")
    input_value.send_keys(calc(x))
    
    button2 = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button2.click()


finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
    